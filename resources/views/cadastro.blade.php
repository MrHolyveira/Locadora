@extends('welcome')

@section("estilos")
    <link href="/assets/css/cadastro.css" rel="stylesheet">
@endsection

@section('content')

    <section class="content-section" style="padding-top: 115px;">


        <div class="container">
            <section class="header"></section>

            <section id="cadastro">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="Nome">Nome completo</label>
                                <input class="form-control" type="text" id="Nome" placeholder="Nome">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="Email">E-mail</label>
                                <input class="form-control" type="email" id="Email" placeholder="E-mail">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="Senha">Senha</label>
                                <input class="form-control" type="password" id="Senha" placeholder="Senha">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="padding-top: 30px">
                            <input type="submit" value="Cadastrar" class="btn btn-primary btn-bloc" style="background-color: transparent; border-color: rgb(66, 220, 163); padding: 0.375rem 5.75rem; color:rgb(66, 220, 163)">
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>

@endsection