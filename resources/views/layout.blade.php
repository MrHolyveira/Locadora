@extends('welcome')
@section('estilos')
    <link href="/assets/css/layout.css" rel="stylesheet">
@endsection

@section('content')
    <!-- Intro Header -->
    <header class="masthead">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <h1 class="brand-heading" style="text-shadow: 5px 5px black">IF GAMES</h1>
                        <p class="intro-text">Alugue seus jogos com uma pitada de nostalgia.</p>
                        <a href="#about" class="btn btn-circle js-scroll-trigger">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section id="about" class="content-section text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h2>Sobre IF GAMES</h2>
                    <p>Aqui você pode alugar jogos de diversas plataformas, enviaremos o jogo na sua casa após efetuado o pagamento.</p>
                    <p>Uma oportunidade perfeita para reviver tempos mais simples e sentir aquela sensação gostosa de nostalgia.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Download Section -->
    <section id="download" class="download-section content-section text-center">
        <div class="container">
            <div class="col-lg-8 mx-auto">
                <h2>VARIEDADE DE JOGOS</h2>
                <p>Confira agora os jogos disponiveis para aluguel, fazemos questão de entrega-lo na sua casa! :)</p>
                <a href="http://startbootstrap.com/template-overviews/grayscale/" class="btn btn-default btn-lg">Conferir jogos</a>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact" class="content-section text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h2>Entre em contato através das nossas redes sociais</h2>
                    <p>Fique à vontade para deixar críticas, sugestões e elogios nas nossas páginas.</p>
                    <ul class="list-inline banner-social-buttons">
                        <li class="list-inline-item">
                            <a href="https://twitter.com" class="btn btn-default btn-lg">
                                <i class="fa fa-twitter fa-fw"></i>
                                <span class="network-name">Twitter</span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://github.com" class="btn btn-default btn-lg">
                                <i class="fa fa-github fa-fw"></i>
                                <span class="network-name">Github</span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://facebook.com" class="btn btn-default btn-lg">
                                <i class="fa fa-facebook" style="margin-right: 5px"></i>
                                <span class="network-name">Facebook</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection